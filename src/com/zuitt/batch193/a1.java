package com.zuitt.batch193;

import java.util.Scanner;

public class a1 {

    public static void main (String[] args) {
        Scanner appScanner = new Scanner(System.in);

        System.out.println("First Name: ");
        String firstName = appScanner.nextLine().trim();

        System.out.println("Last Name: ");
        String lastName = appScanner.nextLine().trim();

        System.out.println("First Subject Grade: ");
        double firstSubject = appScanner.nextDouble();

        System.out.println("Second Subject Grade: ");
        double secondSubject = appScanner.nextDouble();

        System.out.println("Third Subject Grade: ");
        double thirdSubject = appScanner.nextDouble();

        double totalGrade = (firstSubject + secondSubject + thirdSubject) / 3;

        System.out.println("Good day, " + firstName + " " + lastName + ".");
        System.out.println("Your grade average is: " + totalGrade);
    }

}
